import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;


public class GymwolfTests extends TestBase{

    @Test
    public void bodyFatCalculator(){
        SetUp();
        Pay();
        WebDriverWait wait = new WebDriverWait(browser,15);
        browser.findElement(By.linkText("Kalkulaatorid")).click();
        browser.findElement(By.linkText("Keharasva kalkulaator")).click();
        browser.findElement(By.id("gender_male")).click();
        browser.findElement(By.id("height_cm")).sendKeys("180");
        browser.findElement(By.id("height")).sendKeys("1.8");
        browser.findElement(By.id("waist")).sendKeys("100");
        browser.findElement(By.id("neck")).sendKeys("30");
        browser.findElement(By.xpath("/html/body/div[2]/div/div[2]/form/div[6]/div/button")).click();
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/p[1]"),"10.25 %"));
        browser.close();
    }
    @Test
    public void newExercise() throws InterruptedException {
        SetUp();
        WebDriverWait wait = new WebDriverWait(browser,15);
        browser.findElement(By.linkText("Harjutused ja kavad")).click();
        browser.findElement(By.linkText("Treeningkavad")).click();
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/div[3]/div/p"),"Sa ei ole ühtegi treeningkava lisanud. Treeningkavasid saab kasutada alusena uue trenni lisamisel. Lehel \"Uus Trenn\" saad valida eelnevalt salvestatud treeningkava. Kõik valtud treeningkava harjutused lisatakse uuele trennile, kus neid vajadusel muuta saab."));
        browser.findElement(By.linkText("Uus treeningkava")).click();
        browser.findElement(By.id("new_workout_plan_name")).sendKeys("test");
        browser.findElement(By.id("workout_plan_description")).sendKeys("test");
        Random rand = new Random();
        int n = rand.nextInt(5000);
        browser.findElement(By.name("exercise_name[]")).sendKeys("test"+n);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ui-id-3"))).click();
        Thread.sleep(2000);
        browser.findElement(By.xpath("//*[@id=\"add_exercise\"]/div[2]/button")).click();
        Thread.sleep(2000);
        browser.findElement(By.name("weight[0][]")).sendKeys("50");
        browser.findElement(By.name("reps[0][]")).sendKeys("10");
        Thread.sleep(2000);
        browser.findElement(By.name("weight[0][]")).click();
        browser.findElement(By.xpath("/html/body/div[2]/div[3]/div/form[1]/div[4]/button")).click();
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/div[1]/div"),"Treeningkava salvestatud."));
        browser.findElement(By.linkText("Harjutused ja kavad")).click();
        browser.findElement(By.linkText("Treeningkavad")).click();
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/div[3]/div/table[1]/tbody/tr/td/a"),"test"));
        browser.close();
    }
}