import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {
    protected WebDriver browser;
    public static final String Webpage = "https://www.gymwolf.com/staging/";

    public void SetUp(){
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        browser = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(browser,10);
        browser.get(Webpage);
        browser.findElement(By.linkText("Registreeru")).click();
        browser.findElement(By.name("signup_email")).sendKeys("test@test.ee");
        browser.findElement(By.xpath("/html/body/div[1]/div[1]/div[3]/div[3]/form/div[2]/button")).click();
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/div[1]/div"),"test@test.ee"));
    }
    public void Pay(){
        browser.findElement(By.linkText("Kalkulaatorid")).click();
        browser.findElement(By.linkText("Osta Gymwolf Pro")).click();
        browser.findElement(By.xpath("//*[@id=\"payment-paysera-submit-button\"]/button")).click();
        browser.findElement(By.name("correction_amount")).sendKeys("1");
        browser.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[1]/div[3]/form/input[3]")).click();
        WebDriverWait wait = new WebDriverWait(browser,15);
        wait.until(ExpectedConditions.textToBePresentInElement(By.xpath("/html/body/div[2]/p"),"Ost sooritatud"));
    }
}
